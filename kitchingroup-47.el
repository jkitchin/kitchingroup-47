;;; kitchingroup-47.el --- Probing the Coverage Dependence of Site and Adsorbate Configurational Correlations on (111) Surfaces of Late Transition Metals

;; Copyright (C) 2016 John Kitchin

;; Version: 0.0.1
;; Author: Zhongnan Xu
;;   John Kitchin <jkitchin@andrew.cmu.edu>
;; Keywords:
;; DOI: 10.1021/jp508805h
;; Journal: J. Phys. Chem. C
;; Bibtex: @article{xu-2014-probin-cover,
;;   author =	 {Zhongnan Xu and John R. Kitchin},
;;   title =	 {Probing the Coverage Dependence of Site and Adsorbate
;;                   Configurational Correlations on (111) Surfaces of Late
;;                   Transition Metals},
;;   journal =	 {J. Phys. Chem. C},
;;   volume =	 118,
;;   number =	 44,
;;   pages =	 {25597-25602},
;;   year =	 2014,
;;   doi =	 {10.1021/jp508805h},
;;   url =	 {http://dx.doi.org/10.1021/jp508805h},
;;   keywords =	 {DESC0004031, early-career, orgmode, },
;;  }

;;; Commentary:


;;; Code:
(require 'cappa)

(cappa-register 'kitchingroup-47)


(provide 'kitchingroup-47)
;;; kitchingroup-47.el ends here
